<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'id', 'group_id', 'creator_id', 'producer_id', 'completed', 'title', 'description'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    // relations
    public function group(){
        return $this->belongsTo('App\Group');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
