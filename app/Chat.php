<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $fillable = [
        'id', 'message', 'user_id', 'group_id', 'created_at'
    ];

    protected $hidden = [
        'updated_at'
    ];

    // relations
    public function group(){
        return $this->belongsTo('App\Group');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
