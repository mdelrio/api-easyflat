<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $fillable = [
        'id', 'group_id', 'creator_id', 'producer_id', 'completed', 'title', 'description'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    // relations
    public function users(){
        return $this->belongsToMany('App\User');
    }

    public function schedules(){
        return $this->hasMany('App\Schedule');
    }
}
