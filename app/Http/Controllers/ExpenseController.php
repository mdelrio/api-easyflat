<?php

namespace App\Http\Controllers;

use App\Expense;
use App\User;
use Illuminate\Http\Request;

class ExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($group_id)
    {
        return response()->json(['expenses' => Expense::where('group_id', $group_id)->orderBy('created_at', 'DESC')->get()], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $expense = Expense::create([
            'subject' => $request->subject,
            'description' => $request->description,
            'price' => $request->price,
            'group_id' => $request->group_id
        ]);

        $usersId = explode(',', $request->users_id);
        $priceCalculated = $request->price / count($usersId);
        foreach ($usersId as $userId){
            if (auth()->user()->id != $userId)
                User::where('id', $userId)->first()->expenses()->attach($expense->id, ['price' => $priceCalculated*-1]);
            else
                User::where('id', $userId)->first()->expenses()->attach($expense->id, ['price' => ($request->price - $priceCalculated)]);
        }
        if (!in_array(auth()->user()->id, $usersId)) {
            auth()->user()->expenses()->attach($expense->id, ['price' => $request->price]);
        }
        $response = (['expense'=>Expense::where('id', $expense->id)->first()]);
        $status = 200;

        return response()->json($response, $status);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Group $group
     * @return \Illuminate\Http\Response
     */
    public function show(Expense $expense)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $expense = Expense::where('id', $request->id)->first();
        $expense->subject = $request->subject;
        $expense->description = $request->description;
        $expense->price = $request->price;
        $expense->users()->detach();

        if ($expense->save()){
            $usersId = explode(',', $request->users_id);
            $priceCalculated = $request->price / count($usersId);

            foreach ($usersId as $userId){
                if (auth()->user()->id != $userId)
                    User::where('id', $userId)->first()->expenses()->attach($expense->id, ['price' => $priceCalculated*-1]);
                else
                    User::where('id', $userId)->first()->expenses()->attach($expense->id, ['price' => ($request->price - $priceCalculated)]);
            }
            if (!in_array(auth()->user()->id, $usersId)) {
                auth()->user()->expenses()->attach($expense->id, ['price' => $request->price]);
            }
            $message = (['expense' => $expense]);
            $status = 200;
        }
        else{
            $message = (['error' => 'No se pudo editar el gasto']);
            $status = 403;
        }
        return response()->json($message, $status);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Group $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Expense $expense)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Group $group
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $expense = Expense::where('id', $id)->first();
        $expense->users()->detach();
        if($expense->delete()) {
            $message = (['message' => "Correcto"]);
            $status = 200;
        }
        return response()->json($message, $status);
    }
}
