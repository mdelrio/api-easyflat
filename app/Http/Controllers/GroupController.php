<?php

namespace App\Http\Controllers;

use App\Group;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['groups' => auth()->user()->groups], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $group = Group::create([
            'code' => Str::random(16),
            'name' => $request->name
        ]);
        if (auth()->user()->groups()->save($group)) {
            $message = (['group' => $group]);
            $status = 200;
        } else {
            $message = 'El grupo no pudo ser creado';
            $status = 500;
        }
        return response()->json($message, $status);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Group $group
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Group $group
     * @return \Illuminate\Http\Response
     */
    public function edit(Group $group)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Group $group
     * @return \Illuminate\Http\Response
     */
    public function update($code)
    {
        $groupID = Group::where('code', $code)->value('id');
        if ($groupID == null){
            $message = 'El grupo no existe';
            $status = 500;
        }
        elseif (DB::table('group_user')->where('group_id', $groupID)->where('user_id', Auth::id())->count()){
            $message = 'Ya perteneces al grupo';
            $status = 500;
        }
        elseif (auth()->user()->groups()->sync($groupID)) {
            $message = (['group' => Group::where('code', $code)->first()]);
            $status = 200;
        } else {
            $message = 'Error al unirse al grupo';
            $status = 500;
        }
        return response()->json($message, $status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Group $group
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->groups()->detach($id)) {
            $response = (['id' => $id]);
            $status = 200;
            if (DB::table('group_user')->where('group_id', $id)->count() == 0)
                Group::where('id', $id)->delete();
        } else{
            $response = "Error";
            $status = 403;
        }
        return response()->json($response, $status);
    }
}
