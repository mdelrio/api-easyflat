<?php

namespace App\Http\Controllers;

use App\Expense;
use App\Group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DebtController extends Controller
{
    public function getDebts($id){
        $expenses = Expense::where('group_id', $id)->get('id');
        $debts = (['debts' => DB::table('expense_user')->select('id', 'user_id', 'expense_id', 'price')->whereIn('expense_id', $expenses)->get()]);
        return response()->json($debts);
    }
}
