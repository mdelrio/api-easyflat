<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = [
        'id', 'code', 'name'
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'pivot'
    ];

    // relations
    public function users(){
        return $this->belongsToMany('App\User');
    }

    public function messages(){
        return $this->hasMany('App\Chat');
    }
}
