<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $fillable = [
        'id', 'group_id', 'subject', 'description', 'price', 'image', 'created_at'
    ];

    protected $hidden = [
        'updated_at'
    ];

    // relations
    public function group(){
        return $this->belongsTo('App\Group');
    }

    public function users(){
        return $this->belongsToMany('App\User');
    }
}
