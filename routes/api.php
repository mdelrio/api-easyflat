<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Auth::routes(['verify' => true]);

Route::post('login', 'PassportController@login');
Route::post('register', 'PassportController@register');
Route::get('email/verify/{id}', 'VerificationApiController@verify')->name('verificationapi.verify');
Route::get('email/resend’, ‘VerificationApiController@resend')->name('verificationapi.resend');


Route::group(['middleware' => ['web']], function () {
   Route::get('find/{token}', 'PasswordResetController@find');
});
Route::post('create', 'PasswordResetController@create');
Route::post('reset', 'PasswordResetController@reset')->name('password.reset');


Route::middleware('auth:api')->group(function () {
    Route::get('user', 'PassportController@getDetails');
    Route::post('users', 'PassportController@getUsers');
    //ruta: api/user

    Route::get('groups', 'GroupController@index');
    Route::post('groups','GroupController@store');
    Route::put('groups/{code}', 'GroupController@update');
    Route::delete('groups/{id}', 'GroupController@destroy');
    //ruta: api/group

    Route::get('messages/{group_id}', 'MessageController@index');
    Route::post('messages', 'MessageController@store');

    Route::get('expenses/{group_id}', 'ExpenseController@index');
    Route::post('expenses', 'ExpenseController@store');
    Route::post('editexpense', 'ExpenseController@edit');
    Route::delete('expenses/{id}', 'ExpenseController@destroy');

    Route::get('debts/{group_id}', 'DebtController@getDebts');
    Route::post('debts', 'DebtController@getDebtsTotalUser');

    Route::get('logout', 'PassportController@logout');
    //ruta: api/logout
});
